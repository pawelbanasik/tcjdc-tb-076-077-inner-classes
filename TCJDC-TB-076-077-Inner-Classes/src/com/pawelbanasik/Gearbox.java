package com.pawelbanasik;

import java.util.ArrayList;

public class Gearbox {

	private ArrayList<Gear> gears;
	private int maxGears;
	private int currentGear = 0; // zmienil nazwe z gearNumber na currentGear ze
									// wzgledu na przykrywanie
	private boolean clutchIsIn;

	public Gearbox(int maxGears) {
		super();
		this.gears = new ArrayList<>();
		this.maxGears = maxGears;
		Gear neutral = new Gear(0, 0.0);
		this.gears.add(neutral);
		
		for (int i =0; i < maxGears; i++) {
			
			addGear(i, i*5.3);
		}
		
	}

	public void operateClutch(boolean in) {

		this.clutchIsIn = in;
	}

	public void addGear(int number, double ratio) {
		if ((number > 0) && (number <= maxGears)) {
			
			this.gears.add(new Gear(number, ratio));
			
		}
	}

	public void changeGear(int newGear) {
		if ((newGear >= 0) && (newGear < this.gears.size()) && this.clutchIsIn) {
			this.currentGear = newGear;
			System.out.println("Gear " + newGear + " selected.");

		} else {
			System.out.println("Grind");
			this.currentGear = 0;
		}
	}
	
	public double wheelSpeed (int revs) {
		if(clutchIsIn) {
			System.out.println("Scream");
			return 0.0;
		}
		return revs * gears.get(currentGear).getRatio();
		
	}

	// wewnetrzna klasa
	// gearbox i gear tworza pare dlatego to ma zastosowanie
	// gear ma sens tylko wewnatrz z klasa gearbox
	// klasa gear ma dostep do instances and methods of the outer class!!!
	// nawet dostep do prywatnych!!!!
	// gearNumber w klasie gear przykrywa/is shadowing gearNumber w klasie
	// zewnetrznej
	private class Gear {

		private int gearNumber;
		private double ratio;

		public Gear(int gearNumber, double ratio) {
			super();
			this.gearNumber = gearNumber;
			this.ratio = ratio;
		}

		public double driveSpeed(int revs) {

			return revs * (this.ratio);
		}

		public double getRatio() {
			return ratio;
		}

		
		
	}

}
